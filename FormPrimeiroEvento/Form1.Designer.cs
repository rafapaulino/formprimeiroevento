﻿namespace FormPrimeiroEvento
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.botao_vermelho = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_nome = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tb_nomelido = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(123, 225);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(219, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cor de Fundo Amarelo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // botao_vermelho
            // 
            this.botao_vermelho.Location = new System.Drawing.Point(123, 175);
            this.botao_vermelho.Name = "botao_vermelho";
            this.botao_vermelho.Size = new System.Drawing.Size(219, 23);
            this.botao_vermelho.TabIndex = 1;
            this.botao_vermelho.Text = "Cor de Fundo Vermelho";
            this.botao_vermelho.UseVisualStyleBackColor = true;
            this.botao_vermelho.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome:";
            // 
            // tb_nome
            // 
            this.tb_nome.Location = new System.Drawing.Point(150, 12);
            this.tb_nome.Name = "tb_nome";
            this.tb_nome.Size = new System.Drawing.Size(219, 20);
            this.tb_nome.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(112, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(257, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Ler Nome";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // tb_nomelido
            // 
            this.tb_nomelido.Location = new System.Drawing.Point(112, 80);
            this.tb_nomelido.Name = "tb_nomelido";
            this.tb_nomelido.Size = new System.Drawing.Size(257, 20);
            this.tb_nomelido.TabIndex = 5;
            this.tb_nomelido.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 298);
            this.Controls.Add(this.tb_nomelido);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tb_nome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.botao_vermelho);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Eventos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button botao_vermelho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_nome;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tb_nomelido;
    }
}

